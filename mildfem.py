# -*- coding: utf-8 -*-
"""
Created on Thu Nov 16 16:26:52 2017

@author: pmr
"""

import numpy as np
import matplotlib.pyplot as plt
from .fem import fem, tri_shapefuncs, tri_unknwns
from .mesh import get_bndry
from .wave import wave_num, flatbot_wave

class mildfem(fem):
    """
    """
    
    def __init__(self, nds, tri, dpth, prd, order=4):
        """
        """
        
        self.nds = nds
        self.tri = tri
        self.order = order
        self.dpth = dpth
        self.prd = prd
        self.wavnum = wave_num([self.prd,], self.dpth)[0]
        self.cel = 2.*np.pi/self.prd/self.wavnum
        self.cel_grp = .5*self.cel*(1.+2.*self.wavnum*self.dpth/np.sinh(2.*self.wavnum*self.dpth))
        ##
        self.Sommerfeld_bndry = list()
        self.Body_bndry = list()
        self.Body_sys = list()
        ## order of interpolation
        chi, self.Nexp, self.Ncoeff = tri_shapefuncs(self.order)
        self.nds_unk, self.tri_unk = tri_unknwns(self.nds, self.tri, chi, tol=1e-3)
        
    def mild_eqs(self):
        """
        """
        
        self.field_eqs(self.cel*self.cel_grp, self.cel*self.cel_grp*self.wavnum**2, np.zeros(len(self.nds)))
        
    def diff_eqs(self, xc, r, Bdiff, phi0, phi0args=(), inout=True):
        """
        """
        
        ## get boundary
        bndry = get_bndry(self.nds, self.tri, np.array([r*np.cos(np.linspace(0., 2.*np.pi, 50))+xc[0], r*np.sin(np.linspace(0., 2.*np.pi, 50))+xc[1]]).T)
        if inout: bndry = bndry[::-1]
        nds, wavnum, celcel_grp = self.nds[bndry], self.wavnum[bndry].mean(), (self.cel[bndry]*self.cel_grp[bndry]).mean()
        ## order of interpolation
        l1Q, wghtQ = np.polynomial.legendre.leggauss(2*self.order)
        ## continuity of potentials and flow
        num_mds = int((len(Bdiff)-1)/2)
        num_unk = len(self.nds_unk)
        b = np.zeros(2*num_mds+1, complex)
        bp = np.zeros(num_unk, complex)
        MAS = np.zeros((2*num_mds+1, 2*num_mds+1), complex)
        MA = np.zeros((2*num_mds+1, num_unk), complex)
        MpAS = np.zeros((num_unk, 2*num_mds+1), complex)
        for n in range(len(nds)-1):
            ## segment characterisation
            vdir = nds[n+1]-nds[n]
            Lngth = np.sqrt(vdir.dot(vdir))
            vdir /= Lngth
            nrm = np.array([-vdir[1], vdir[0]])
            lQ = Lngth/2.*l1Q+Lngth/2. # (b-a)/2.*x+(b+a)/2.
            xQ = nds[n]+vdir*np.repeat(lQ, 2).reshape((2*self.order, 2))
            ## triangle characterisation
            cond = np.any(self.tri == bndry[n+1], axis=1)*np.any(self.tri == bndry[n], axis=1)
            t = np.where(cond)[0][0]
            tr = self.tri[t]
            xtr = self.nds[tr] # nodes
            M = (xtr[:2]-xtr[2]).T # x-x2 = M.dot(chi)
            detM = M[0, 0]*M[1, 1]-M[0, 1]*M[1, 0]
            Minv = 1./detM*np.array([[xtr[1, 1]-xtr[2, 1], -(xtr[1, 0]-xtr[2, 0])], [-(xtr[0, 1]-xtr[2, 1]), xtr[0, 0]-xtr[2, 0]]], float) # chi = Minv.dot(x-x2)
            chiQ = (Minv.dot((xQ-xtr[2]).T)).T.reshape((-1, 1, 2))
            ## shape functions
            psiQ = (chiQ[:, :, 0]**self.Nexp[:, 0]*chiQ[:, :, 1]**self.Nexp[:, 1]).dot(self.Ncoeff)
            ## diffraction condition
            psidiff, psidiff_x, psidiff_y = flatbot_wave(xQ, np.array([xc,]), np.array([wavnum,]), np.array([num_mds,]), np.array([2,]), np.array([wavnum*r,]), angl=np.array([0.,]), output_grad=True)
            psidiff, psidiff_x, psidiff_y = psidiff.T, psidiff_x.T, psidiff_y.T 
            if not np.all(Bdiff==0.):
                psip, psip_x, psip_y = flatbot_wave(xQ, np.array([xc,]), np.array([wavnum,]), np.array([num_mds,]), np.array([1,]), np.array([wavnum*r,]), angl=np.array([0.,]), output_grad=True)
                psidiff, psidiff_x, psidiff_y = psip.T+Bdiff.T.dot(psidiff), psip_x.T+Bdiff.T.dot(psidiff_x), psip_y.T+Bdiff.T.dot(psidiff_y)                
            qth = np.arange(-num_mds, num_mds+1), np.arctan2(xQ[:, 1]-xc[1], xQ[:, 0]-xc[0])
            qth = np.meshgrid(qth[0], qth[1], indexing='ij')
            qth = (qth[0]*qth[1]).reshape((-1, 1, len(xQ)))
            phi, phi_n = phi0(*((xQ, nrm)+phi0args))
            ## equations
            unk = self.tri_unk[t]
            b += -Lngth/2.*(phi*np.exp(-1j*qth[:, 0, :])).dot(wghtQ)
            bp[unk] += -Lngth/2.*(celcel_grp*phi_n*psiQ.T).dot(wghtQ)
            psiQ, psidiff = psiQ.T.reshape((1, -1, len(xQ))), psidiff.reshape((1, -1, len(xQ)))
            MAS += Lngth/2.*(psidiff*np.exp(-1j*qth)).dot(wghtQ)
            MA[:, unk] += Lngth/2.*(psiQ*np.exp(-1j*qth)).dot(wghtQ)
            psiQ = psiQ[0].reshape((-1, 1, len(xQ)))
            MpAS[unk, :] += -Lngth/2.*(celcel_grp*(psidiff_x*nrm[0]+psidiff_y*nrm[1])*psiQ).dot(wghtQ)
        ##
        MASinv = np.linalg.solve(MAS, np.eye(*MAS.shape))
        self.bsys += MpAS.dot(MASinv.dot(b))+bp
        self.Msys += -MpAS.dot(MASinv.dot(MA))
        
        return bndry, MASinv, MA, b
        
    def smmrfld_eqs(self, rS, phiS0, phiS0args=(), num_mds=20):
        """
        """
        
        bndry = self.diff_eqs(np.array([0., 0.]), rS, np.zeros((num_mds, num_mds)), phiS0, phi0args=phiS0args, inout=True)[0]
        self.Sommerfeld_bndry.append(self.nds[bndry])
        
    def body_eqs(self, xcB, rB, Bdiff, phirad, phiradargs=()):
        """
        """
        
        bndry, MASinv, MA, b = self.diff_eqs(xcB, rB, Bdiff, phirad, phi0args=phiradargs, inout=False)
        self.Body_bndry.append(self.nds[bndry])
        self.Body_sys.append([MASinv.dot(MA), -MASinv.dot(b)])
        
    def show(self):
        """
        """
        
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.triplot(self.nds[:, 0], self.nds[:, 1], self.tri)
        lgnd = ['Mesh','']
        ax.plot(self.nds_unk[:, 0], self.nds_unk[:, 1], 'o')
        lgnd.append('Degrees of Freedom')
        ##
        for Sbndry in self.Sommerfeld_bndry:
            vdir = ((Sbndry[1:]-Sbndry[:-1]).T/np.sqrt(((Sbndry[1:]-Sbndry[:-1])**2).sum(axis=1))).T
            vnrm = np.array([-vdir[:, 1], vdir[:, 0]]).T
            ax.plot(Sbndry[:, 0], Sbndry[:, 1], '-')
            ax.quiver(.5*Sbndry[1:, 0]+.5*Sbndry[:-1, 0], .5*Sbndry[1:, 1]+.5*Sbndry[:-1, 1], vnrm[:, 0], vnrm[:, 1], units='width')
#            lgnd.append('Sommerfeld Boundary')
#            lgnd.append('Normal Vectors')
        ##
        for bndry in self.Body_bndry:
            vdir = ((bndry[1:]-bndry[:-1]).T/np.sqrt(((bndry[1:]-bndry[:-1])**2).sum(axis=1))).T
            vnrm = np.array([-vdir[:, 1], vdir[:, 0]]).T
            ax.plot(bndry[:, 0], bndry[:, 1], '-')
            ax.quiver(.5*bndry[1:, 0]+.5*bndry[:-1, 0], .5*bndry[1:, 1]+.5*bndry[:-1, 1], vnrm[:, 0], vnrm[:, 1], units='width')
#            lgnd.append('Body Boundary')
#            lgnd.append('Normal Vectors')
        ##
        ax.legend(lgnd)
        ax.set_aspect('equal', 'datalim')
