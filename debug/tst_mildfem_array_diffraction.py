# -*- coding: utf-8 -*-
"""
"""

## add top_level parent package
import sys
import os
sys.path.append(os.path.join('..', '..'))

## imports
import numpy as np
from scipy.special import jvp, h1vp, jv, hankel1
import matplotlib.pyplot as plt
from mild_slope.mildfem import mildfem
from mild_slope.mesh import mesh_array
from mild_slope.wave import wave_num, plane_wave

## wave conditions
dpthS = 20.
prd = 5.
wavnumS = wave_num([prd,], [dpthS,])[0, 0]
angl = 0.*np.pi/180.

## radius wave domain and bodies
rfS, rfB = 50., 5.

## bodies coordinates
coords = np.arange(-1, 2)*rfB*5.
coords = np.meshgrid(coords, coords, indexing='ij')
coords = np.array([coords[0].flatten(), coords[1].flatten()]).T

## mesh array
length_triangles = rfB*1.25
num_pnts_in_length = [3, 2, 2, 2, 2, 2, 2, 2, 2]
nds, tri = mesh_array(coords, length_triangles, num_pnts_in_length, rfS, rfB, show_msh=False, quadrant_lim=[0, 3])

## bathymetry
dpth = dpthS*np.ones(len(nds)) # constant water depth

## mild slope equation - Finite Element Method
order = 2
imsef = mildfem(nds, tri, dpth, prd, order=order)
imsef.mild_eqs()

## add Sommerfeld boundary condition (open sea condition)
imsef.smmrfld_eqs(rfS, plane_wave, phiS0args=(wavnumS, 2.*np.pi/prd, angl), num_mds=20)

## add 3D bodies boundary conditions (diffraction/radiation)
for coord in coords:
    ## dummy radiation wave
    def phirad(pnts, nrm):
        return np.zeros(len(pnts)), np.zeros(len(pnts)) # phi, grad_phi.dot(nrm)
    ## diffraction transfer matrix
    num_mds = 5 
    nu = np.arange(-num_mds, num_mds+1)
    dpthB = dpth[((nds-coord)**2).sum(axis=1)==((nds-coord)**2).sum(axis=1).min()][0]
    wavnumB = wave_num([prd,], [dpthB,])[0, 0]
    Bdiff = np.diag(-jvp(nu, wavnumB*rfB)/h1vp(nu, wavnumB*rfB))
    Bdiff *= np.diag(hankel1(nu, wavnumB*rfB)/jv(nu, wavnumB*rfB)) # normalisation
    ## add 3D body boundary condition
    imsef.body_eqs(coord, rfB, Bdiff, phirad, phiradargs=())
    
## visualise problem
imsef.show()

## solve system of equations imsef.Msys.dot(af) = imsef.bsys
af = np.linalg.solve(imsef.Msys, imsef.bsys)

## solve for incident amplitude coefficients at body i, ab[i] = imsef.Body_sys[i][0].dot(af)+imsef.Body_sys[i][1]
ab = np.array([imsef.Body_sys[i][0].dot(af)+imsef.Body_sys[i][1] for i in range(len(coords))], complex)

## after solving for ab, forces at body i are calculated simply by:
# forces[i] = Force_Transfer_Matrix[i].dot(ab[i]);
# where Force_Transfer_Matrix[i].shape = (number of degrees of freedom, 2*num_mds+1).

## points for latter free surface visualisation
Np = 100
pnts = np.meshgrid(np.linspace(-rfS, rfS, Np), np.linspace(-rfS, rfS, Np), indexing='ij')
pnts = np.array([pnts[0].flatten(), pnts[1].flatten()]).T

## evaluate solution at points
psif = imsef.trial_func(pnts, output_grad=False)[0]
etaf = 1j*2.*np.pi/prd/9.809*psif.dot(af)

## visualise free surface
rpnts = np.sqrt((pnts**2).sum(axis=1))
cond = rpnts > rfS
for coord in coords:
    cond += ((pnts-coord)**2).sum(axis=1) < rfB**2
vmin = np.abs(etaf[cond==False]).min()*0.95
vmax = np.abs(etaf[cond==False]).max()*1.05
fig = plt.figure()
ax = fig.add_subplot(111)
etaf[cond] = np.nan
cp = ax.contour(pnts[:, 0].reshape((Np, Np)), pnts[:, 1].reshape((Np, Np)), np.abs(etaf).reshape((Np, Np)), np.linspace(vmin, vmax, 10), linewidths=.75, colors='k')
cpf = ax.contourf(pnts[:, 0].reshape((Np, Np)), pnts[:, 1].reshape((Np, Np)), np.abs(etaf).reshape((Np, Np)), np.linspace(vmin, vmax, 10))
ax.set_aspect('equal', 'datalim')
fig.colorbar(cpf, ax=ax)
