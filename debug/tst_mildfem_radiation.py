# -*- coding: utf-8 -*-
"""
"""

## add top_level parent package
import sys
import os
sys.path.append(os.path.join('..', '..'))

## imports
import numpy as np
from scipy.special import jvp, h1vp
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import spsolve
import matplotlib.pyplot as plt
from matplotlib.tri import Triangulation
from mild_slope.mildfem import mildfem
from mild_slope.mesh import cloud_circ_cnst_sep
from mild_slope.wave import wave_num, flatbot_wave

## wave conditions
dpth = 50.
prd = 8.
wavnum = wave_num([prd,], [dpth,])[0, 0]
angl = 0.*np.pi/180.

## mesh circle
rfS, NrS = 250., 5
rfI = rfS*.33333 
nds = np.vstack([cloud_circ_cnst_sep(rfI, rfS, NrS, th0=0., thf=2.*np.pi), [0., 0.]])
tri = Triangulation(nds[:, 0], nds[:, 1]).triangles
cntrs = nds[tri].mean(axis=1)
cond = (cntrs**2).sum(axis=1) >= rfI**2
tri = tri[cond]

## bathymetry
dpth = dpth*np.ones(len(nds), float)

## grid of points for latter free surface visualisation
Np = 100
pnts = np.meshgrid(np.linspace(-rfS, rfS, Np), np.linspace(-rfS, rfS, Np), indexing='ij')
pnts = np.array([pnts[0].flatten(), pnts[1].flatten()]).T

##
phiS0 = lambda x, n: (np.zeros(len(x)), np.zeros(len(x)))
def phirad(x, n, wavnum):
    ampl = np.array([20, 50, 20])
    psi, psi_x, psi_y = flatbot_wave(x, np.array([[0., 0.],]), np.array([wavnum,]), np.array([1,]), np.array([2,]), np.array([-1.,]), angl=np.array([0.,]), output_grad=True)
    return ampl.dot(psi.T), ampl.dot(psi_x.T)*n[0]+ampl.dot(psi_y.T)*n[1]
mds = np.arange(-5, 5+1)
Bdiff = np.diag(-jvp(mds, wavnum*rfI)/h1vp(mds, wavnum*rfI))

## mild slope equation - Finite Element Method
order = 4
imsef = mildfem(nds, tri, dpth, prd, order=order)
imsef.mild_eqs()
imsef.smmrfld_eqs(rfS, phiS0, phiS0args=(), num_mds=20)
imsef.body_eqs(np.array([0., 0.]), rfI, Bdiff, phirad, phiradargs=(wavnum,))
imsef.show()
af = spsolve(csr_matrix(imsef.Msys), imsef.bsys)
ab = imsef.Body_sys[0][0].dot(af)+imsef.Body_sys[0][1] # incident amplitude coefficients at body 0
# forces = Force_Transfer_Matrix.dot(ab)
psif = imsef.trial_func(pnts, output_grad=False)[0]
etaf = 1j*2.*np.pi/prd/9.809*psif.dot(af)

## mild slope equation - Exact Solution
psiI = phirad(pnts, np.array([0., 0.]), wavnum)[0]
etae = 1j*2.*np.pi/prd/9.809*psiI

## visualise wave
rpnts = np.sqrt((pnts**2).sum(axis=1))
cond = (rpnts < rfI) + (rpnts > rfS)
vmin = min([np.abs(etaf[cond==False]).min(), np.abs(etae[cond==False]).min()])*0.95
vmax = max([np.abs(etaf[cond==False]).max(), np.abs(etae[cond==False]).max()])*1.05
fig, axes = plt.subplots(nrows=1, ncols=2)
axes = axes.flatten()
for i, etai in enumerate([etaf, etae]):
    etai[cond] = np.nan
    cp = axes[i].contour(pnts[:, 0].reshape((Np, Np)), pnts[:, 1].reshape((Np, Np)), np.abs(etai).reshape((Np, Np)), np.linspace(vmin, vmax, 10), linewidths=.75, colors='k')
    cpf = axes[i].contourf(pnts[:, 0].reshape((Np, Np)), pnts[:, 1].reshape((Np, Np)), np.abs(etai).reshape((Np, Np)), np.linspace(vmin, vmax, 10))
    axes[i].set_aspect('equal', 'datalim')
fig.colorbar(cpf, ax=axes.ravel().tolist())
