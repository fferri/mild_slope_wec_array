# -*- coding: utf-8 -*-
"""
"""

import numpy as np
from scipy.special import hankel1, jv, yv, h1vp, jvp, yvp

class isl_shoal(object):
    
    def __init__(self, depth0, depth1, radius0, radius1, dimensionless, omega, alpha, order=5, grav=9.809):
        """
        depth0 is the water depth after the shoal (if dimensionless, use xi0)
        depth1 is the water depth in the island (if dimensionless, use xi1)
        radius0 is the radius of the shoal (if dimensionless, use rho0)
        radius1 is the radius of the island (if dimensionless, use rho1)
        dimensionless (bool) are depth and radius input dimensionless?
        omega is the circular wave frequency
        alpha defines the shape of the bottom
        order is the truncation order for the infinite series
        grav is the acceleration of gravity
        """
        
        ## convert
        if dimensionless:
            depth0 = np.sign(depth0)*depth0**2/omega**2*grav
            depth1 = np.sign(depth1)*depth1**2/omega**2*grav
            radius0 = np.sign(radius0)*radius0/omega**2*grav
            radius1 = np.sign(radius1)*radius1/omega**2*grav
        
        ## missing param
        if depth0 < 0.:
            depth0 = depth1*(radius0/radius1)**alpha
        elif depth1 < 0.:
            depth1 = depth0*(radius1/radius0)**alpha
        elif radius0 < 0.:
            radius0 = radius1*(depth0/depth1)**(1./alpha)
        else:
            radius1 = radius0*(depth1/depth0)**(1./alpha)

        ## generate atributes
        self.depth0 = depth0
        self.depth1 = depth1
        self.radius0 = radius0
        self.radius1 = radius1
        self.omega = omega
        self.alpha = alpha
        self.order = order
        self.grav = grav
        
        ## other param
        self.param()
        
    def param(self):
        """
        """
        
        ind = np.arange(self.order+1)
        epsilon = 2.*np.ones(ind.shape, float)
        epsilon[0] = 1.
        xi0 = self.omega*np.sqrt(self.depth0/self.grav)
        if xi0 >= .310:
            print('Warning!!! xi0 = {:.3f} >= 0.310'.format(xi0))
        rho0 = self.omega**2*self.radius0/self.grav
        self.xi0 = xi0
        self.beta = rho0**(self.alpha/2.)/xi0
        Psi0 = hankel1(ind, rho0/xi0)
        Psip0 =  1./xi0*h1vp(ind, rho0/xi0)
        Phi0 = jv(ind, rho0/xi0)
        Phip0 = 1./xi0*jvp(ind, rho0/xi0)
        R10 = self.radialfunc(rho0*np.ones(ind.shape), ind, 1)
        R20 = self.radialfunc(rho0*np.ones(ind.shape), ind, 2)
        R1p0 = self.radialfunc(rho0*np.ones(ind.shape), ind, 3)
        R2p0 = self.radialfunc(rho0*np.ones(ind.shape), ind, 4)
        rho1 = self.omega**2*self.radius1/self.grav
        R1p1 = self.radialfunc(rho1*np.ones(ind.shape), ind, 3)
        R2p1 = self.radialfunc(rho1*np.ones(ind.shape), ind, 4)
        self.R1p1 = R1p1
        self.R2p1 = R2p1
        Gamma = R2p1*R10-R1p1*R20
        Lambda = R2p1*R1p0-R1p1*R2p0
        
        self.Ccoeff = -1j**ind*epsilon*(Psi0*Phip0-Psip0*Phi0)/(Gamma*Psip0-Lambda*Psi0)
        self.Dcoeff = -1j**ind*epsilon*(Gamma*Phip0-Lambda*Phi0)/(Gamma*Psip0-Lambda*Psi0)
        
    def freesurface(self, radius, theta, ampl=1.):
        """
        ampl is the amplitude of the incident plane wave
        wnum0 is wavenumber over depth0
        """
        
        ind = np.arange(self.order+1)
        RADIUS, IND = np.meshgrid(radius, ind, indexing='ij')
        THETA, IND = np.meshgrid(theta, ind, indexing='ij')
        
        RHO = self.omega**2*RADIUS/self.grav
        PSI = hankel1(IND, RHO/self.xi0)
        R1 = self.radialfunc(RHO, IND, 1)
        R2 = self.radialfunc(RHO, IND, 2)
        
        cond1 = radius > self.radius0
        cond2 = radius <= self.radius0
        
        wnum0 = self.omega/np.sqrt(self.grav*self.depth0)
        eta = np.zeros(radius.shape, complex)
        eta[cond1] = (ampl*np.exp(1j*wnum0*radius*np.cos(theta)))[cond1]
        eta[cond1] += ((PSI*np.cos(IND*THETA)).dot(ampl*self.Dcoeff))[cond1.reshape(-1)]
        eta[cond2] = (((self.R2p1*R1-self.R1p1*R2)*np.cos(IND*THETA)).dot(ampl*self.Ccoeff))[cond2.reshape(-1)]
        
        return eta
        
    def radialfunc(self, rho, ind, ID):
        """
        R = C1*R(ID==1)+C2*R(ID==2)
        Rp = C1*R(ID==3)+C2*R(ID==4)
        """
        if self.alpha == 2:
            delta = 1+ind**2-self.beta**2
            if type(rho) == int or type(rho) == float or type(rho) == np.int32 or type(rho) == np.float64:
                delta = np.array([delta,])
                rho = np.array([rho,])
            R = rho**-1.
            mu = np.sqrt(np.abs(delta))
            deltaneg = delta < 0
            deltapos = delta > 0
            delta0 = delta == 0
            if ID == 1:
                R[deltaneg] *= np.cos(mu*np.log(rho))[deltaneg]
                R[delta0] *= 1.
                R[deltapos] *= (rho**mu)[deltapos]
            elif ID == 2:
                R[deltaneg] *= np.sin(mu*np.log(rho))[deltaneg]
                R[delta0] *= np.log(rho)[delta0]
                R[deltapos] *= (rho**-mu)[deltapos]
            elif ID == 3:
                R[deltaneg] *= (-rho**-1.*(np.cos(mu*np.log(rho))+mu*np.sin(mu*np.log(rho))))[deltaneg]
                R[delta0] *= (-rho**-1)[delta0]
                R[deltapos] *= ((-1.+mu)*rho**(-1.+mu))[deltapos]
            elif ID == 4:
                R[deltaneg] *= (rho**-1.*(mu*np.cos(mu*np.log(rho))-np.sin(mu*np.log(rho))))[deltaneg]
                R[delta0] *= (rho**-1.*(-np.log(rho)+1.))[delta0]
                R[deltapos] *= ((-1.-mu)*rho**(-1.-mu))[deltapos]
            if type(rho) == int or type(rho) == float or type(rho) == np.int32 or type(rho) == np.float64:
                R = R[0]
        else:
            R = rho**(-self.alpha/2.)
            nu = np.sqrt(self.alpha**2+4*ind**2)/np.abs(2.-self.alpha)
            arg = 2.*self.beta/np.abs(2.-self.alpha)*rho**(1-self.alpha/2.)
            argp = (1-self.alpha/2.)*2.*self.beta/np.abs(2.-self.alpha)*rho**(-self.alpha/2.)
            if ID == 1:
                R *= jv(nu, arg)
            elif ID == 2:
                R *= yv(nu, arg)
            elif ID == 3:
                R *= -self.alpha/2.*rho**-1.*jv(nu, arg)+argp*jvp(nu, arg)
            elif ID == 4:
                R *= -self.alpha/2.*rho**-1.*yv(nu, arg)+argp*yvp(nu, arg)                
        
        return R
