# -*- coding: utf-8 -*-
"""
"""

## add top_level parent package
import sys
import os
sys.path.append(os.path.join('..', '..'))

## imports
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.tri import Triangulation
from mild_slope.mildfem import mildfem
from mild_slope.mesh import cloud_circ_cnst_sep
from mild_slope.wave import wave_num, plane_wave
from mild_slope.debug.isl_shoal import isl_shoal

## domain and wave conditions
rfI = 1e4
dpthS, dpthI = 4e3, 4e2
alpha = 2.
rfS = rfI*(dpthS/dpthI)**(1./alpha)
prd = 240. # 240., 480., 720.
wavnumS, wavnumI = wave_num([prd,], [dpthS,])[0, 0], wave_num([prd,], [dpthI,])[0, 0]
angl = 0.*np.pi/180.

## mesh circle
NrS = 5
nds = np.vstack([cloud_circ_cnst_sep(rfI, rfS, NrS, th0=0., thf=2.*np.pi), [0., 0.]])
tri = Triangulation(nds[:, 0], nds[:, 1]).triangles
cntrs = nds[tri].mean(axis=1)
cond = (cntrs**2).sum(axis=1) >= rfI**2
tri = tri[cond]

## bathymetry
rnds = np.sqrt((nds**2).sum(axis=-1))
slp = (dpthS-dpthI)/(rfS**alpha-rfI**alpha)
dpth = slp*(rnds**alpha-rfI**alpha)+dpthI
cond = rnds < rfI
dpth[cond] = dpthI

## grid of points for latter free surface visualisation
Np = 100
pnts = np.meshgrid(np.linspace(-rfS, rfS, Np), np.linspace(-rfS, rfS, Np), indexing='ij')
pnts = np.array([pnts[0].flatten(), pnts[1].flatten()]).T

## mild slope equation - Finite Element Method
order = 3
imsef = mildfem(nds, tri, dpth, prd, order=order)
imsef.mild_eqs()
imsef.smmrfld_eqs(rfS, plane_wave, phiS0args=(wavnumS, 2.*np.pi/prd, angl), num_mds=20)
imsef.show()
af = np.linalg.solve(imsef.Msys, imsef.bsys)
psif = imsef.trial_func(pnts, output_grad=False)[0]
etaf = 1j*2.*np.pi/prd/9.809*psif.dot(af)

## mild slope equation - Exact Solution
rpnts, thpnts = np.sqrt((pnts**2).sum(axis=1)), np.arctan2(pnts[:, 1], pnts[:, 0])
cond = (rpnts < rfI) + (rpnts > rfS)
iisl = isl_shoal(dpthS, dpthI, -1., rfI, False, 2.*np.pi/prd, alpha, order=5, grav=9.809)
etae = np.zeros(rpnts.shape, complex)
etae[cond==False] = iisl.freesurface(rpnts[cond==False], thpnts[cond==False], ampl=1.)

## visualise wave
rpnts = np.sqrt((pnts**2).sum(axis=1))
cond = (rpnts < rfI) + (rpnts > rfS)
vmin = min([np.abs(etaf[cond==False]).min(), np.abs(etae[cond==False]).min()])*0.95
vmax = max([np.abs(etaf[cond==False]).max(), np.abs(etae[cond==False]).max()])*1.05
fig, axes = plt.subplots(nrows=1, ncols=2)
axes = axes.flatten()
for i, etai in enumerate([etaf, etae]):
    etai[cond] = np.nan
    cp = axes[i].contour(pnts[:, 0].reshape((Np, Np)), pnts[:, 1].reshape((Np, Np)), np.abs(etai).reshape((Np, Np)), np.linspace(vmin, vmax, 10), linewidths=.75, colors='k')
    cpf = axes[i].contourf(pnts[:, 0].reshape((Np, Np)), pnts[:, 1].reshape((Np, Np)), np.abs(etai).reshape((Np, Np)), np.linspace(vmin, vmax, 10))
    axes[i].set_aspect('equal', 'datalim')
fig.colorbar(cpf, ax=axes.ravel().tolist())
