# -*- coding: utf-8 -*-
"""
"""

## add top_level parent package
import sys
import os
sys.path.append(os.path.join('..', '..'))

## imports
import numpy as np
from scipy.special import jvp, h1vp
import matplotlib.pyplot as plt
from matplotlib.tri import Triangulation
from mild_slope.mildfem import mildfem
from mild_slope.mesh import cloud_circ_cnst_sep
from mild_slope.wave import wave_num, plane_wave, flatbot_wave

## wave conditions
dpth = 50.
prd = 8.
wavnum = wave_num([prd,], [dpth,])[0, 0]
angl = 0.*np.pi/180.

## mesh circle
rfS, NrS = 250., 5
rfI = rfS*.33333 
nds = np.vstack([cloud_circ_cnst_sep(rfI, rfS, NrS, th0=0., thf=2.*np.pi), [0., 0.]])
tri = Triangulation(nds[:, 0], nds[:, 1]).triangles
cntrs = nds[tri].mean(axis=1)
cond = (cntrs**2).sum(axis=1) >= rfI**2
tri = tri[cond]

## bathymetry
dpth = dpth*np.ones(len(nds), float)

## grid of points for latter free surface visualisation
Np = 100
pnts = np.meshgrid(np.linspace(-rfS, rfS, Np), np.linspace(-rfS, rfS, Np), indexing='ij')
pnts = np.array([pnts[0].flatten(), pnts[1].flatten()]).T

## mild slope equation - Finite Element Method
order = 4
imsef = mildfem(nds, tri, dpth, prd, order=order)
imsef.mild_eqs()
imsef.smmrfld_eqs(rfS, plane_wave, phiS0args=(wavnum, 2.*np.pi/prd, angl), num_mds=20)
imsef.show()
af = np.linalg.solve(imsef.Msys, imsef.bsys)
psif = imsef.trial_func(pnts, output_grad=False)[0]
etaf = 1j*2.*np.pi/prd/9.809*psif.dot(af)

## mild slope equation - Exact Solution
Nm = 5
mds = np.arange(-Nm, Nm+1)
Bdiff = np.diag(-jvp(mds, wavnum*rfI)/h1vp(mds, wavnum*rfI))
A = Bdiff.dot(-1j*9.809/(2.*np.pi/prd)*np.exp(1j*mds*(np.pi/2-angl)))
psiI = flatbot_wave(pnts, np.array([[0., 0.]]), np.array([wavnum,]), np.array([Nm,]), np.array([2,]), np.array([-1.,]), np.array([angl,]), output_grad=False)[0]
etae = 1j*2.*np.pi/prd/9.809*plane_wave(pnts, np.zeros(2), wavnum, 2.*np.pi/prd, angl)[0]+1j*2.*np.pi/prd/9.809*psiI.dot(A)

## visualise wave
rpnts = np.sqrt((pnts**2).sum(axis=1))
cond = (rpnts < rfI) + (rpnts > rfS)
vmin = min([np.abs(etaf[cond==False]).min(), np.abs(etae[cond==False]).min()])*0.95
vmax = max([np.abs(etaf[cond==False]).max(), np.abs(etae[cond==False]).max()])*1.05
fig, axes = plt.subplots(nrows=1, ncols=2)
axes = axes.flatten()
for i, etai in enumerate([etaf, etae]):
    etai[cond] = np.nan
    cp = axes[i].contour(pnts[:, 0].reshape((Np, Np)), pnts[:, 1].reshape((Np, Np)), np.abs(etai).reshape((Np, Np)), np.linspace(vmin, vmax, 10), linewidths=.75, colors='k')
    cpf = axes[i].contourf(pnts[:, 0].reshape((Np, Np)), pnts[:, 1].reshape((Np, Np)), np.abs(etai).reshape((Np, Np)), np.linspace(vmin, vmax, 10))
    axes[i].set_aspect('equal', 'datalim')
fig.colorbar(cpf, ax=axes.ravel().tolist())
