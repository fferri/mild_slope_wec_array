# -*- coding: utf-8 -*-
"""
Created on Tue Nov 14 15:30:50 2017

@author: pmr
"""

import numpy as np
from .mesh import remove_repeats
from .integrl import tri_gauss_pnts

def tri_shapefuncs(order):
    """
    """
    
    pull = np.tril_indices(order+1)
    
    ## nodes
    chi = np.linspace(0., 1., order+1)
    chi = np.meshgrid(chi, chi, indexing='ij')
    chi[0] = chi[0][::-1]
    chi = np.array([chi[0][pull], chi[1][pull]]).T
    
    ## polynomials = chi**poly_exp[:, 0]*eta**poly_exp[:, 1], i.e., (chi**0*eta**0, chi**0*eta**1, chi**1*eta**0, chi**0*eta**2, chi**1*eta**1, chi**2*eta**0, ...)
    poly_exp = np.arange(order+1)
    poly_exp = np.meshgrid(poly_exp, poly_exp, indexing='ij')
    poly_exp = np.array([poly_exp[1][pull], (poly_exp[0]-poly_exp[1])[pull]]).T
    
    ## coefficients for shape functions
    unk = int(((order+1+1)*(order+1))/2) # unk = sum_{i=1}^{order+1} i
    chi = chi.reshape((unk, 1, 2))
    poly_coeff = np.linalg.solve(chi[:, :, 0]**poly_exp[:, 0]*chi[:, :, 1]**poly_exp[:, 1], np.eye(unk))
    ## poly_coeff[:, i] are the coeffs of the shape function associated with chi[i, :], i.e., N_i
    ## N_i(chi[i, :]) = delta_ij
    ## N_i(chi, eta) = (chi**poly_exp[:, 0]*eta**poly_exp[:, 1]).dot(poly_coeff[:, i])
                  
    return chi[:, 0, :], poly_exp, poly_coeff

def tri_unknwns(nds, tri, chi, tol=1e-3):
    """
    """
    
    nds_unk = list()
    tri_unk = np.zeros((len(tri), len(chi)), int)
    for t, tr in enumerate(tri):
        tri_unk[t] = np.arange(len(chi))+len(nds_unk)
        x = nds[tr] # nodes
        M = (x[:2]-x[2]).T # x-x2 = M.dot(chi)
        nds_unk += list(M.dot(chi.T).T+x[2]) # chi nodes
    nds_unk = np.array(nds_unk, float)
    nds_unk, tri_unk = remove_repeats(nds_unk, tri_unk, tol=tol)
    
    return nds_unk, tri_unk
    
class fem(object):
    """
    """
    
    def __init__(self, nds, tri, order=4):
        """
        """
        
        self.nds = nds
        self.tri = tri
        self.order = order
        self.bndry = list()
        ## order of interpolation
        chi, self.Nexp, self.Ncoeff = tri_shapefuncs(self.order)
        self.nds_unk, self.tri_unk = tri_unknwns(self.nds, self.tri, chi, tol=1e-3)
    
    def field_eqs(self, A, B, f):
        """      
        div(A*grad(phi)) + B*phi = f
        
        phi = sum_{i}^{len(nds_unk)} phi_i*psi_i:
        for j = 1, ..., len(nds_unk):
            sum_{i}^{len(nds_unk)} phi_i*int_{Area} B*psi_i*psi_j-A*grad(psi_i).dot(grad(psi_j)) d{Area} =
            = int_{Area} f*psi_j d{Area} - int_{Boundary} A*grad(phi)*norm{Boundary} d{Boundary}
        Msys.dot([phi_1, ..., phi_len(nds_unk)]) = bsys
        
        A.shape = (len(nds), )
        B.shape = (len(nds), )
        f.shape = (len(nds), )
        """
        
        ## order of interpolation
        self.A, self.B, self.f = A, B, f
        chiQ, wghtQ = tri_gauss_pnts(2*self.order) # psi_i*psi_j
        chiQGr, wghtQGr = tri_gauss_pnts([2*(self.order-1), 1][self.order==1]) # grad(psi_i).dot(grad(psi_j))
        ## shape functions
        chiQ = chiQ.reshape((-1, 1, 2))
        chiQGr = chiQGr.reshape((-1, 1, 2))
        psi = (chiQ[:, :, 0]**self.Nexp[:, 0]*chiQ[:, :, 1]**self.Nexp[:, 1]).dot(self.Ncoeff)
        cnd = self.Nexp[:, 0]-1 >= 0
        psi_chi = (self.Nexp[cnd, 0]*chiQGr[:, :, 0]**(self.Nexp[cnd, 0]-1)*chiQGr[:, :, 1]**self.Nexp[cnd, 1]).dot(self.Ncoeff[cnd])
        cnd = self.Nexp[:, 1]-1 >= 0
        psi_eta = (self.Nexp[cnd, 1]*chiQGr[:, :, 1]**(self.Nexp[cnd, 1]-1)*chiQGr[:, :, 0]**self.Nexp[cnd, 0]).dot(self.Ncoeff[cnd])
        chiQ, chiQGr = chiQ[:, 0, :], chiQGr[:, 0, :]
        ## equations
        num_unk = len(self.nds_unk)
        self.bsys = np.zeros(num_unk, complex)
        self.Msys = np.zeros((num_unk, num_unk), complex)
        for t, tr in enumerate(self.tri):
            ## triangle characterisation
            x = self.nds[tr] # nodes
            BQ = (B[tr][:2]-B[tr][2]).dot(chiQ.T)+B[tr][2] # B-B2 = MB.dot(chi)
            AQGr = (A[tr][:2]-A[tr][2]).dot(chiQGr.T)+A[tr][2] # A-A2 = MA.dot(chi)
            M = (x[:2]-x[2]).T # x-x2 = M.dot(chi)
            detM = M[0, 0]*M[1, 1]-M[0, 1]*M[1, 0]
            area = .5*np.abs(detM)
            Minv = 1./detM*np.array([[x[1, 1]-x[2, 1], -(x[1, 0]-x[2, 0])], [-(x[0, 1]-x[2, 1]), x[0, 0]-x[2, 0]]], float) # chi = Minv.dot(x-x2)
            grad_psi = np.array([psi_chi*Minv[0, 0]+psi_eta*Minv[1, 0], psi_chi*Minv[0, 1]+psi_eta*Minv[1, 1]]).T
            ## equations
            eq, unk = np.meshgrid(self.tri_unk[t], self.tri_unk[t], indexing='ij')
            self.Msys[eq, unk] += area*(BQ*wghtQ*psi.T).dot(psi)
            self.Msys[eq, unk] += -area*(AQGr*wghtQGr*grad_psi[:, :, 0]).dot(grad_psi[:, :, 0].T)
            self.Msys[eq, unk] += -area*(AQGr*wghtQGr*grad_psi[:, :, 1]).dot(grad_psi[:, :, 1].T)
            if np.any(f[tr] != 0.):
                fQ = (f[tr][:2]-f[tr][2]).dot(chiQ.T)+f[tr][2]
                self.bsys[eq] += area*(fQ*wghtQ*psi.T).sum(axis=1)
                         
    def boundary_eqs(self, bndry, g, D, gargs=(), Dargs=()):
        """
        Boundary condition grad(phi)*normal=g+D*phi along the set of lines
        from nds[:-1] to nds[1:]
        
        g = function/method whose inputs are a set of points and normal vector, g(pnts, nrm), 
            and a set of arguments, gargs, and output is the value of the 
            function at those points, pnts[0], ..., pnts[Np-1] = [x0, y0], ..., [xNp-1, yNp-1]
        D = function/method inputs are a set of points and normal vector, D(pnts, nrm), 
            and a set of arguments, Dargs, and output is the value of the 
            function at those points, pnts[0], ..., pnts[Np-1] = [x0, y0], ..., [xNp-1, yNp-1]
        """
        
        ## order of interpolation
        l1Q, wghtQ = np.polynomial.legendre.leggauss(self.order) # psi_j
        l1QQ, wghtQQ = np.polynomial.legendre.leggauss(2*self.order) # psi_i*psi_j
        ## equations
        x = self.nds[bndry]
        for n in range(len(x)-1):
            ## segment characterisation
            vdir = x[n+1]-x[n]
            Lngth = np.sqrt(vdir.dot(vdir))
            vdir /= Lngth
            nrm = np.array([-vdir[1], vdir[0]])
            lQ = Lngth/2.*l1Q+Lngth/2. # (b-a)/2.*x+(b+a)/2.
            xQ = x[n]+vdir*np.repeat(lQ, 2).reshape((self.order, 2))
            lQQ = Lngth/2.*l1QQ+Lngth/2. # (b-a)/2.*x+(b+a)/2.
            xQQ = x[n]+vdir*np.repeat(lQQ, 2).reshape((2*self.order, 2))
            seg = [bndry[n], bndry[n+1]]
            AQ = (self.A[seg][:1]-self.A[seg][1])*lQ+self.A[seg][1]
            AQQ = (self.A[seg][:1]-self.A[seg][1])*lQQ+self.A[seg][1]
            ## triangle characterisation
            cond = np.any(self.tri == bndry[n+1], axis=1)*np.any(self.tri == bndry[n], axis=1)
            t = np.where(cond)[0][0]
            tr = self.tri[t]
            xtr = self.nds[tr] # nodes
            M = (xtr[:2]-xtr[2]).T # x-x2 = M.dot(chi)
            detM = M[0, 0]*M[1, 1]-M[0, 1]*M[1, 0]
            Minv = 1./detM*np.array([[xtr[1, 1]-xtr[2, 1], -(xtr[1, 0]-xtr[2, 0])], [-(xtr[0, 1]-xtr[2, 1]), xtr[0, 0]-xtr[2, 0]]], float) # chi = Minv.dot(x-x2)
            chiQ = (Minv.dot((xQ-xtr[2]).T)).T.reshape((-1, 1, 2))
            chiQQ = (Minv.dot((xQQ-xtr[2]).T)).T.reshape((-1, 1, 2))
            ## shape functions
            psiQ = (chiQ[:, :, 0]**self.Nexp[:, 0]*chiQ[:, :, 1]**self.Nexp[:, 1]).dot(self.Ncoeff)
            psiQQ = (chiQQ[:, :, 0]**self.Nexp[:, 0]*chiQQ[:, :, 1]**self.Nexp[:, 1]).dot(self.Ncoeff)
            ## equations
            eq, unk = np.meshgrid(self.tri_unk[t], self.tri_unk[t], indexing='ij')
            self.bsys[eq] += -Lngth/2.*(AQ*g(*((xQ, nrm)+gargs))*wghtQ*psiQ.T).sum(axis=1) # (b-a)/2.*f(x).dot(wght)
            self.Msys[eq, unk] += -Lngth/2.*(AQQ*D(*((xQQ, nrm)+Dargs))*wghtQQ*psiQQ.T).dot(psiQQ)
        self.bndry.append(x)
        
    def trial_func(self, pnts, output_grad=True):
        """
        """
        
        psi = np.zeros((len(pnts), len(self.nds_unk)), float)
        psi_x = np.zeros((len(pnts), len(self.nds_unk)), float)
        psi_y = np.zeros((len(pnts), len(self.nds_unk)), float)
        for t, tr in enumerate(self.tri):
            xtr = self.nds[tr]
            M = (xtr[:2]-xtr[2]).T # x-x2 = M.dot(chi)
            detM = M[0, 0]*M[1, 1]-M[0, 1]*M[1, 0]
            Minv = 1./detM*np.array([[xtr[1, 1]-xtr[2, 1], -(xtr[1, 0]-xtr[2, 0])], [-(xtr[0, 1]-xtr[2, 1]), xtr[0, 0]-xtr[2, 0]]], float) # chi = Minv.dot(x-x2)
            chi = (Minv.dot((pnts-xtr[2]).T)).T
            pnt_in = np.where((chi[:, 1] <= 1.-chi[:, 0])*(chi[:, 0] >= 0.)*(chi[:, 1] >= 0.))[0]
            chi = chi[pnt_in, :].reshape((-1, 1, 2))
            unk = self.tri_unk[t]
            pnt_in, unk = np.meshgrid(pnt_in, unk, indexing='ij')
            psi[pnt_in, unk] = (chi[:, :, 0]**self.Nexp[:, 0]*chi[:, :, 1]**self.Nexp[:, 1]).dot(self.Ncoeff)
            if output_grad:
                cnd = self.Nexp[:, 0]-1 >= 0
                psi_chi = (self.Nexp[cnd, 0]*chi[:, :, 0]**(self.Nexp[cnd, 0]-1)*chi[:, :, 1]**self.Nexp[cnd, 1]).dot(self.Ncoeff[cnd])
                cnd = self.Nexp[:, 1]-1 >= 0
                psi_eta = (self.Nexp[cnd, 1]*chi[:, :, 1]**(self.Nexp[cnd, 1]-1)*chi[:, :, 0]**self.Nexp[cnd, 0]).dot(self.Ncoeff[cnd])
                psi_x[pnt_in, unk] = psi_chi*Minv[0, 0]+psi_eta*Minv[1, 0]
                psi_y[pnt_in, unk] = psi_chi*Minv[0, 1]+psi_eta*Minv[1, 1]
        
        return psi, psi_x, psi_y
