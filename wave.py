# -*- coding: utf-8 -*-
"""
Created on Mon Nov 06 11:43:54 2017

@author: pmr
"""

import numpy as np
from scipy.special import jv, hankel1, jvp, h1vp

def dispersion(L, const):
    """
    Linear dispersion equation, f=0
    """
    
    T, h = const
    A = 9.809*T**2/2./np.pi
    B = 2.*np.pi*h
    f = L-A*np.tanh(B/L)
    df = 1-(A*B*(np.tanh(B/L)**2-1.))/L**2
           
    return f,df

def newton(func, const, x=1., feat=(100, 1e-6, 1e-6)):
    """
    Equation: func=f(x)=0
    Constants: tuple with func constants
    First iterate: x
    Max. number of iterations, Tolerance in x and f: feat
    """
    
    kmax, Tol_x, Tol_f = feat
    found = 0
    k = 0
    while found == 0 and k < kmax:
        k += 1 # Update k
        # Evaluating f and df
        f, df = func(x,const)
        # Avoid dividing by zero
        if abs(df) < Tol_f*1e-3:
            found = 1
            bl = (0, 'Local minimum. log|f| = {:.2f}'.format(np.log10(abs(f)+1e-99)))
        else:
            # Update x
            x0 = x
            x += -f/df
            # Check on convergence
            if abs(x-x0) < Tol_x and abs(f) < Tol_f:
                found= 1
                bl= (1, 'Converged in {} iterations and with log|f| = {:.2f}'.format(k, np.log10(abs(f)+1e-99)))
    # Did not converge
    if found == 0:
        bl= (0, 'Did not converge. log|f| = {:.2f}'.format(np.log10(abs(f)+1e-99)))
        
    return x, bl

def wave_num(period, depth):
    """
    Calculates wave-number, k0, from wave-period and water depth (T,h).
    Dispersion relationship for travelling modes
    Inputs:
    - period: numpy array or list
    - depth: numpy array or list
    """
    
    length = np.zeros((len(period), len(depth)), float)
    for iT, T in enumerate(period):
        for ih, h in enumerate(depth):
            length[iT, ih] = newton(dispersion, (T, h))[0]
            
    return 2.*np.pi/length

def plane_wave(pnts, nrm, wavnum, freq, angl):
    """
    """
    
    r, th = np.sqrt((pnts**2).sum(axis=1)), np.arctan2(pnts[:, 1], pnts[:, 0])
    phi = -1j*9.809/freq*np.exp(1j*wavnum*r*np.cos(th-angl))
    phi_r = 1j*wavnum*np.cos(th-angl)*phi
    phi_th = 1j*wavnum*r*-np.sin(th-angl)*phi
    
    return phi, (phi_r*np.cos(th)-1./r*phi_th*np.sin(th))*nrm[0]+(phi_r*np.sin(th)+1./r*phi_th*np.cos(th))*nrm[1]

def bess(n, x, H=False, p=False):
    """
    """
    
    try:
        N = n.max()
    except ValueError:
        return np.zeros(n.shape, [float, complex][H])
    val = np.zeros(n.shape, [float, complex][H])
    for nu in range(N+1):
        cnd = (n>=0)*(np.abs(n)==nu)
        val[cnd] = [[jv, jvp][p], [hankel1, h1vp][p]][H](n[cnd], x[cnd])
        if nu > 0:
            cnd_sym = (n<0)*(np.abs(n)==nu)
            val[cnd_sym] = val[cnd]*(-1)**nu
    return val
    
def flatbot_wave(pnts, cntrs, wnumcntrs, num_mds, kind, norm_lngths, angl, output_grad=True):
    """        
    Evaluation points: pnts[0], ..., pnts[Np-1] = [x0, y0], ..., [xNp-1, yNp-1]
    Centers of plane/cylindrical waves: cntrs[0], ..., cntrs[N-1] = [x0, y0], ..., [xN-1, yN-1]
    Wave number: wnumcntrs[0], ..., wnumcntrs[N-1]
    Number of plane/cylindrical waves: num_mds[0], ..., num_mds[N-1]
    Kind of waves: kind[0], ..., kind[N-1] (kind=0:plane; kind=1:incoming; kind=2:outgoing)
    Normalisation lengths: norm_lngths[0], ..., norm_lngths[N-1]
    Angle of plane waves: angl[0], ..., angl[N-1] (angl[i] if kind[i]!=0 is irrelevant)
    Output the gradient?: output_grad
    """
    
    ## shapes preparation
    Np, N, Nm = len(pnts), len(num_mds), num_mds.max()
    pnts = pnts.reshape((Np, 1, 2))
    th = np.arctan2(pnts[:, :, 1]-cntrs[:, 1], pnts[:, :, 0]-cntrs[:, 0]) # shape = (Np, N)
    r = np.sqrt(((pnts-cntrs)**2).sum(axis=2)) # shape = (Np, N)
    m = np.tile(np.arange(-Nm, Nm+1), N) # shape = (N*(2*Nm+1))
    kind = np.repeat(kind, 2*Nm+1) # shape = (N*(2*Nm+1))
    cut = np.abs(m) <= np.repeat(num_mds, 2*Nm+1)
    cut[(kind==0)*(m<=0)] = False # -Nm, -Nm+1, ..., 0 are not valid modes for plane waves
    m = np.tile(m[cut], Np).reshape((Np, -1)) # shape (Np, sum(cut))
    kind = np.tile(kind[cut], Np).reshape((Np, -1)) # shape m.shape
    k = np.tile(np.repeat(wnumcntrs, 2*Nm+1)[cut], Np).reshape((Np, -1)) # shape m.shape
    KR = np.tile(np.repeat(norm_lngths, 2*Nm+1)[cut], Np).reshape((Np, -1)) # shape m.shape
    angl = np.tile(np.repeat(angl, 2*Nm+1)[cut], Np).reshape((Np, -1)) # shape m.shape
    th = np.repeat(th, 2*Nm+1).reshape((Np, -1))[:, cut] # shape m.shape
    r = np.repeat(r, 2*Nm+1).reshape((Np, -1))[:, cut] # shape m.shape
    ##
    pln = kind == 0
    cyl_in = kind == 1
    cyl_out = kind == 2
    cyl = cyl_in+cyl_out
    expth = np.exp(1j*m[cyl]*th[cyl])
    psi_nrm_P = np.exp(1j*m[pln][KR[pln]>0.]*KR[pln][KR[pln]>0.])
    psi_nrm_jv = bess(m[cyl_in][KR[cyl_in]>0.], KR[cyl_in][KR[cyl_in]>0.])
    psi_nrm_hnk = bess(m[cyl_out][KR[cyl_out]>0.], KR[cyl_out][KR[cyl_out]>0.], H=True)
    ## psi
    psi = np.zeros(m.shape, complex)
    psi[pln] = np.exp(1j*m[pln]*k[pln]*r[pln]*np.cos(th[pln]-angl[pln]))
    psi[cyl_in] = bess(m[cyl_in], k[cyl_in]*r[cyl_in])
    psi[cyl_out] = bess(m[cyl_out], k[cyl_out]*r[cyl_out], H=True)
    psi[pln*(KR>0.)] /= psi_nrm_P
    psi[cyl_in*(KR>0.)] /= psi_nrm_jv
    psi[cyl_out*(KR>0.)] /= psi_nrm_hnk
    psi[cyl] *= expth
    ## gradient(phi)
    psi_r = np.zeros(m.shape, complex)
    if output_grad:
        psi_r[pln] = 1j*m[pln]*k[pln]*np.cos(th[pln]-angl[pln])*psi[pln]
        psi_r[cyl_in] = k[cyl_in]*bess(m[cyl_in], k[cyl_in]*r[cyl_in], p=True)
        psi_r[cyl_out] = k[cyl_out]*bess(m[cyl_out], k[cyl_out]*r[cyl_out], H=True, p=True)
        psi_r[cyl_in*(KR>0.)] /= psi_nrm_jv
        psi_r[cyl_out*(KR>0.)] /= psi_nrm_hnk
        psi_r[cyl] *= expth
    psi_th = 1j*m*psi
    psi_th[pln] *= k[pln]*r[pln]*-np.sin(th[pln]-angl[pln])
    
    return psi, psi_r*np.cos(th)-1./r*psi_th*np.sin(th), psi_r*np.sin(th)+1./r*psi_th*np.cos(th)        
    