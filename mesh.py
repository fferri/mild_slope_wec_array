# -*- coding: utf-8 -*-
"""
Created on Mon Nov 06 11:43:54 2017

@author: pmr
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.tri import Triangulation

def cloud_circ(r0, rf, Nr, Nth, th0=0., thf=2.*np.pi):
    """
    Generate a cloud of points within the circle r0 <= radius <= rf
    and th0 <= theta <= thf
    
    Nr: Number of equispaced points between radius=r0 and radius=rf
    Nth: Number of equispaced points between theta=th0 and theta=thf
    """
    
    r = np.linspace(r0, rf, Nr)[[0, 1][r0==0.]:] # the case r==0. is not considered for angular discretisation
    th = np.linspace(th0, thf, Nth, endpoint=thf!=2.*np.pi)
    r, th = np.meshgrid(r, th, indexing='ij')
    r, th = r.flatten(), th.flatten()
    xy = np.zeros((len(r)+[0, 1][r0==0.], 2), float) # add the case r==0, i.e., nds[-1] = [0., 0.]
    xy[[0, 1][r0==0.]:, 0] = r*np.cos(th)
    xy[[0, 1][r0==0.]:, 1] = r*np.sin(th)
    
    return xy

def cloud_circ_cnst_sep(r0, rf, Nr, th0=0., thf=2.*np.pi):
    """
    Generate a cloud of points within the circle r0 <= radius <= rf
    and th0 <= theta <= thf keeping a constant separation distance between
    adjacent points that equal to dr = (rf-r0)/(Nr-1)
    
    Nr: Number of equispaced points between radius=r0 and radius=rf
    Nth: Is taken as function of radius so that separation between adjacent points
    is dr = (rf-r0)/(Nr-1) 
    """
    
    dr = (rf-r0)/(Nr-1.)
    r = np.linspace(r0, rf, Nr)[[0, 1][r0==0.]:] # the case r==0. is not considered for angular discretisation
    Nths = np.array((thf-th0)*r/dr, int)+1
    xy = np.zeros((sum(Nths)+[0, 1][r0==0.], 2), float) # add the case r==0, i.e., nds[-1] = [0., 0.]
    ini = 0
    for ir, Nth in enumerate(Nths):
        th = np.linspace(th0, thf, Nth, endpoint=thf!=2.*np.pi)
        xy[ini:ini+Nth] = np.array([r[ir]*np.cos(th), r[ir]*np.sin(th)]).T
        ini += Nth
    
    return xy

def get_bndry(nds, tri, pnts):
        """
        Draw continuous boundary of a triangulation according to a set of reference points
        
        Set of points: pnts
        """
        
        N = len(nds)
        nds = nds.reshape((N, 1, 2))
        dist = np.sqrt(((nds-pnts)**2).sum(axis=2))
        ind = np.argsort(dist, axis=0)[0] # get closest node to point
        Ni = len(ind)
        nds = nds[:, 0, :]
        bndry = list()
        for i in range(Ni-1):
            if i>0:
                if ind[i] == bndry[-1]: 
                    continue # avoid repetition
            if np.any(tri==ind[i]):
                bndry.append(ind[i]) # if the node is in triangulation append
            else: 
                continue # if the node is not in triangulation move to next
            while True:
                tri_i = np.where(tri==bndry[-1])[0] # triangle that contains the node
                dist = np.sqrt(((nds[tri[tri_i]]-nds[ind[i+1]])**2).sum(axis=2))
                indclsest = tri[tri_i][dist==dist.min()][0]
                if indclsest == ind[i+1]:
                    break
                else:
                    bndry.append(indclsest)
        if np.any(tri==ind[Ni-1]) and ind[Ni-1] != bndry[-1]:
            bndry.append(ind[Ni-1])
        
        return np.array(bndry, int)
    
def remove_repeats(nds, tri, tol=1e-3):
    """
    """
    
    rep_bag = list()
    nds_bag = list()
    for n, nd in enumerate(nds):
        if n in rep_bag:
            continue
        nds_bag.append(nd)
        reps = np.where((np.abs(nds[:, 0]-nd[0])<tol)*(np.abs(nds[:, 1]-nd[1])<tol))[0]
        cond = tri==n
        if len(reps) > 1:
            for rep in reps:
                cond += tri==rep
                rep_bag.append(rep)
        tri[cond] = len(nds_bag)-1
    
    return np.array(nds_bag, float), tri

def mesh_array(coord_bdies, length_triangles, num_pnts_in_length, radius_outter, radius_bdies, show_msh=False, quadrant_lim=[0, 3]):
    """
    num_pnts_in_length is a list of integers. each element of the list defines the number of pnts
    that will be used to discretise length_triangles. The length of the list depends on successive
    refinements from coord_bdies to outter boundary. For example, num_pnts_in_length = [3, 2] means that half way
    from bdies to outter boundary triangles will be characterised by a length that equals to length_triangles/num_pnts_in_length[0],
    the half part closest to the bodies, and to length_triangles/num_pnts_in_length[1], the half part closest to the outter boundary.
    """
    
    ths = np.linspace(0., 3.*np.pi/2., 4)[quadrant_lim[0]:quadrant_lim[1]+1] # quadrants
    arg = length_triangles/(num_pnts_in_length[-1]-1)/2./radius_outter
    dth = 2.*np.arcsin(min([arg, 1.]))
    Nth = int((ths[-1]+np.pi/2.-ths[0])/dth+1)
    cloud = cloud_circ_rect(coord_bdies, length_triangles, num_pnts_in_length, radius_outter, Nth, quadrant_lim=quadrant_lim)
    arg = length_triangles/(num_pnts_in_length[0]-1)/2./radius_bdies
    dth = 2.*np.arcsin(min([arg, 1.]))
    Nth = int((ths[-1]+np.pi/2.-ths[0])/dth+1)
    cloud = cloud_circ_bdy(cloud, coord_bdies, radius_bdies, Nth, (quadrant_lim[0]==0)*(quadrant_lim[1]==2))
    nds, cnct = gen_mesh(cloud, coord_bdies, show=show_msh)
    
    return nds, cnct

def cloud_circ_rect(cntrs, dl, Nps, rsmf, Nthsmf, quadrant_lim=[0, 3]):
    """
    quadrant 0: from 0. to 90. deg ; quadrant 1: from 90. to 180. ; 
    quadrant 2: from 180. to 270. ; quadrant 3: from 270. to 360.
    """
    
    ths = np.linspace(0., 3.*np.pi/2., 4)[quadrant_lim[0]:quadrant_lim[1]+1] # quadrants
    th = np.linspace(ths[0], ths[-1]+np.pi/2., Nthsmf, endpoint=quadrant_lim[1]-quadrant_lim[0]!=3) # outter boundary discretisation
    Nps = np.array(Nps, int) # it should be decreasing
    extd = 1.2 # extend mesh extd times the circular outter boundary
    # generate regular grid with spacing equal to dl
    Np = int(rsmf*extd/dl)+1
    pntsq = np.meshgrid(np.arange(Np)*dl, np.arange(Np)*dl, indexing='ij')
    pntsq = np.array([pntsq[0].flatten(), pntsq[1].flatten()]).T
    # generate squares mesh
    ind = np.arange(Np-1)
    sqrq = np.array([ind, ind+1, ind+1+Np, ind+Np]).T
    sqrq = np.array([sqrq+Np*i for i in range(Np-1)], int).reshape((-1, 4))
    # get rid of squares which all nodes are outside the circular outter boundary
    sqrq = sqrq[np.any(pntsq[sqrq][:, :, 0]**2+pntsq[sqrq][:, :, 1]**2<=rsmf**2, axis=1)]
    # complete for all quadrants
    pnts, sqr = [[],], [[]]
    for alpha in ths:
        ROT = np.array([[np.cos(alpha), np.sin(alpha)], [-np.sin(alpha), np.cos(alpha)]])
        sqr += list(sqrq+len(pnts)-1)
        pnts += list(pntsq.dot(ROT))
    pnts, sqr = np.array(pnts[1:]), np.array(sqr[1:])
    # calculate squares' centers and minimum distance between squares' centers and bodies' centers and outter boundary
    cntrsqr = pnts[sqr].mean(axis=1)
    dist = np.array([(cntrsqr[:, 0]-cntr[0])**2+(cntrsqr[:, 1]-cntr[1])**2 for cntr in cntrs]).min(axis=0)
    distout = rsmf-np.sqrt((cntrsqr**2).sum(axis=1))
    # normalise distances from 0 to len(Np)-1
    dist = np.array((dist-dist.min())/(dist.max()-dist.min())*(len(Nps)-1), int)
    # discretise squares according to the different refinements
    pntsrefine = [[],]
    for n in range(len(sqr)):
        xmin, ymin = pnts[sqr[n]].max(axis=0) 
        xmax, ymax = pnts[sqr[n]].min(axis=0)
        Npnout = [2, int(dl/(th[1]-th[0])/rsmf)+1][distout[n]<=rsmf*(extd-1.)]
        Npn = max([Nps[dist[n]], Npnout])
        pntssqr = np.meshgrid(np.linspace(xmin, xmax, Npn), np.linspace(ymin, ymax, Npn), indexing='ij')
        pntssqr = np.array([pntssqr[0].flatten(), pntssqr[1].flatten()]).T
        pntsrefine += list(pntssqr)
    pntsrefine = np.array(pntsrefine[1:])
    # discard points outside the circular outter boundary
    dmin = min([dl/(Nps[0]-1.), (th[1]-th[0])*rsmf])
    dist = pntsrefine[:, 0]**2+pntsrefine[:, 1]**2
    pntsrefine = pntsrefine[dist<(rsmf-dmin)**2]
    # delete duplicates
    pntsrefine = remove_duplicates(pntsrefine)
    # include circular outter boundary
    arc = np.array([rsmf*np.cos(th), rsmf*np.sin(th)]).T
    arc_add = list()
    for arcp in arc:
        dist = (arcp[0]-pntsrefine[:, 0])**2+(arcp[1]-pntsrefine[:, 1])**2
        if dist.min() <= dmin**2:
            pnt_id = np.arange(len(pntsrefine))[dist == dist.min()][0]
            pntsrefine[pnt_id] = arcp
        else:
            arc_add.append(arcp)
    pntsrefine = np.array(list(pntsrefine)+arc_add)
    
    return pntsrefine

def cloud_circ_bdy(nodesOut, cntrs, rI, NthI, sym_x):
    """
    """
    
    th = [np.linspace(0., 2*np.pi, NthI, endpoint=False), np.linspace(0., np.pi, NthI/2)]
    dth = th[0][1]
    # nodes "outside the bodies"
    cond = np.all([((nodesOut-cntr)**2).sum(axis=1)-(rI+dth*rI)**2 >= 0 for cntr in cntrs], axis=0)
    # bordering points
    ndsbord = [[],]
    for cntr in cntrs:
        thIn = th[sym_x*(cntr[1]==0)]
        ndsbord += list(np.array([rI*np.cos(thIn), rI*np.sin(thIn)]).T+cntr)
    # all nodes
    nodes = np.array(list(nodesOut[cond])+ndsbord[1:]+list(cntrs))
    
    return nodes

def gen_mesh(nodes, cntrs, show=False):
    """
    """
    
    # delete duplicates
    nodes = remove_duplicates(nodes)
    # triangulate using Delaunay triangulation
    tri = Triangulation(nodes[:, 0], nodes[:, 1])
    # delete panels inside the bodies
    if len(cntrs) > 0:
        mask = np.any([np.any(np.all(nodes[tri.triangles]==cntr, axis=2), axis=1) for cntr in cntrs], axis=0)
        tri.set_mask(np.where(mask, 1, 0))
        conectivity = tri.triangles[mask!=True]
    else:
        conectivity = tri.triangles
    # remove cntrs
    for cntr in cntrs:
        n = np.arange(len(nodes))[np.all(nodes==cntr, axis=1)][0]
        nodes = nodes[np.arange(len(nodes))!=n]
        conectivity[conectivity>=n] -= 1
    # show
    if show:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.triplot(tri, 'b-')
        ax.set_aspect('equal', 'datalim')
    
    return nodes, conectivity

def remove_duplicates(pnts, Tolx=1e-3):
    """
    """
    
    ok = np.ones(len(pnts), bool)
    for ip, pnt in enumerate(pnts):
        cond = (np.arange(len(pnts))!=ip)*ok
        dist = np.sqrt((((pnt-pnts[cond])**2).sum(axis=1))).min()
        ok[ip] = dist>Tolx
          
    return pnts[ok] 
